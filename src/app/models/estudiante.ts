import { Distrito } from './distrito';

export class Estudiante {
  id: number;
  dni: string;
  nombre: string;
  apellido: string;
  sexo: string;
  fechaNac: string;
  direccion: string;
  telefono: string;
  correo: string;
  distrito: Distrito;
}
