import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Distrito } from '../models/distrito';

@Injectable({
  providedIn: 'root'
})
export class DistritoService {

  private BASE_URL:string = 'https://makemiracle-test.herokuapp.com';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http:HttpClient) {}

  getDistritos():Observable<Distrito[]>{
    return this.http.get<Distrito[]>(`${this.BASE_URL}/app/estudiantes/distritos`);
  }
}
