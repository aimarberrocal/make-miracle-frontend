import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Estudiante } from '../models/estudiante';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  private BASE_URL:string = 'https://makemiracle-test.herokuapp.com';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http:HttpClient) {}

  getEstudiantes():Observable<Estudiante[]>{
    return this.http.get<Estudiante[]>(`${this.BASE_URL}/app/estudiantes`);
  }

  saveEstudiante(estudiante:Estudiante):Observable<any>{
    return this.http.post(`${this.BASE_URL}/app/estudiantes`,estudiante,{headers: this.httpHeaders});
  }

  getEstudiante(id:number):Observable<Estudiante>{
    return this.http.get<Estudiante>(`${this.BASE_URL}/app/estudiantes/${id}`);
  }
  
  /*
  getProducto(id:number):Observable<Producto>{
    return this.http.get<Producto>(`${this.urlEndPoint}/${id}`);
  }
  
  create(producto: Producto): Observable<Producto> {
    return this.http.post<Producto>(this.urlEndPoint, producto, { headers: this.httpHeaders }).pipe(
      map((response: any) => response.producto as Producto),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        let error = e.error.error;
        if (error.search("Duplicate") >= 0) {
          Swal.fire('Nombre Duplicado',`El nombre '${producto.descripcion}' ya se encuentra registrado.`,'warning');
        }
        //Swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  update(producto: Producto): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${producto.id}`, producto, { headers: this.httpHeaders }).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        let error = e.error.error;
        if (error.search("Duplicate") >= 0) {
          Swal.fire('Nombre Duplicado',`El nombre '${producto.descripcion}' ya se encuentra registrado.`,'warning');
        }
        Swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }
  */
}
