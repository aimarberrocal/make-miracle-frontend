import { Component, OnInit } from '@angular/core';
import { DistritoService } from 'src/app/services/distrito.service';
import { Distrito } from '../../models/distrito';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EstudianteService } from 'src/app/services/estudiante.service';
import { Estudiante } from 'src/app/models/estudiante';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss'],
})
export class StudentComponent implements OnInit {
  
  modalVisible = false;
  isChecked = false;
  currentYear: number = new Date().getFullYear();
  tabIndex: number;
  studentForm: FormGroup;
  distritos: Distrito[];
  estudiantes: Estudiante[];
  estudiante: Estudiante = null;

  months = [
    { name: 'Enero', days: 31 },
    { name: 'Febrero', days: 29 },
    { name: 'Marzo', days: 31 },
    { name: 'Abril', days: 30 },
    { name: 'Mayo', days: 31 },
    { name: 'Junio', days: 30 },
    { name: 'Julio', days: 31 },
    { name: 'Agosto', days: 31 },
    { name: 'Septiembre', days: 30 },
    { name: 'Octubre', days: 31 },
    { name: 'Noviembre', days: 30 },
    { name: 'Diciembre', days: 31 },
  ];

  days: number[] = [];

  get dni() {
    return this.studentForm.get('dni');
  }
  get nombre() {
    return this.studentForm.get('nombre');
  }
  get apellido() {
    return this.studentForm.get('apellido');
  }
  get sexo() {
    return this.studentForm.get('sexo');
  }
  get distrito() {
    return this.studentForm.get('distrito');
  }
  get year() {
    return this.studentForm.get('year');
  }
  get telefono() {
    return this.studentForm.get('telefono');
  }

  constructor(
    private fb: FormBuilder,
    private distritoService: DistritoService,
    private estudianteService: EstudianteService,
    private modal: NzModalService
  ) {}

  ngOnInit(): void {
    //Default days init
    for (let i = 1; i <= 29; i++) this.days.push(i);

    this.studentForm = this.fb.group({
      dni: [
        '',
        [Validators.required, Validators.maxLength(8), Validators.minLength(8)],
      ],
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      fechaNac: [null, []],
      distrito: [null, [Validators.required]],
      year: [
        null,
        [
          Validators.max(this.currentYear),
          Validators.min(1950),
          // Validators.pattern('/^([0-9]*)$/m'),
        ],
      ],
      month: [null, []],
      day: [null, []],
      direccion: ['', []],
      telefono: ['', [Validators.maxLength(9), Validators.minLength(9)]],
      correo: ['', []],
    });

    this.listDistricts();
    this.listStudents();
  }

  submitForm(): void {
    for (const i in this.studentForm.controls) {
      this.studentForm.controls[i].markAsDirty();
      this.studentForm.controls[i].updateValueAndValidity();
    }

    console.log(this.studentForm);

    if (this.studentForm.valid) {
      this.generateDate();
      this.createStudent();
    } else {
      console.error('Faltan campos por completar');
      this.modal.warning({
        nzTitle: 'Formulario Incompleto.',
        nzContent: 'Se encontraron campos sin completar.',
      });
    }
  }

  listStudents() {
    this.estudianteService.getEstudiantes().subscribe((data) => {
      console.log(data);
      this.estudiantes = data;
    });
  }

  createStudent() {
    this.estudianteService.saveEstudiante(this.studentForm.value).subscribe(
      (data) => {
        console.warn(data.mensaje);
        this.modal.success({
          nzTitle: 'Formulario Válido.',
          nzContent: data.mensaje,
        });
        this.ngOnInit();
        if (!this.isChecked) this.tabIndex = 0;
        this.studentForm.reset();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  moreInfo(id:number){
    this.estudianteService.getEstudiante(id).subscribe(
      data => {
        this.estudiante = data;
        this.modalVisible = true;
      },
      error => {
        console.error(error);
      }
    )
  }
  modalOk(){
    this.modalVisible = false;
  }
  modalCancel(){
    this.modalVisible = false;
  }
  
  listDistricts() {
    this.distritoService.getDistritos().subscribe((data) => {
      console.log(data);
      this.distritos = data;
    });
  }

  generateDate() {
    let year = this.studentForm.get('year').value;
    let month = this.studentForm.get('month').value;
    let day = this.studentForm.get('day').value;

    if (!year || !month || !day) return;

    if (month < 10) month = `0${month}`;
    if (day < 10) day = `0${day}`;

    this.studentForm.get('fechaNac').setValue(`${year}-${month}-${day}`);

    delete this.studentForm.value.year;
    delete this.studentForm.value.month;
    delete this.studentForm.value.day;
  }

  changeMonth(value: number) {
    if (!value) return;
    let daysMonth = this.months[value - 1].days;
    this.days = [];
    for (let index = 1; index <= daysMonth; index++) this.days.push(index);
  }
}
